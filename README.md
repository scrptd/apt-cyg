# apt-cyg

apt-cyg is a command-line installer for Cygwin which cooperates with Cygwin Setup and uses the same repository. The syntax is similar to apt-get. Usage examples:

* `apt-cyg install <package names>` to install packages
* `apt-cyg remove <package names>` to remove packages
* `apt-cyg update` to update setup.ini
* `apt-cyg show` to show installed packages
* `apt-cyg find <pattern(s)>` to find packages matching patterns
* `apt-cyg describe <pattern(s)>` to describe packages matching patterns
* `apt-cyg packageof <commands or files>` to locate parent packages 

## Download

...
wget <a href='https://source.apt-cyg.com'>https://source.apt-cyg.com</a> -O apt-cyg
...

## Quick start

apt-cyg is a simple script. Once you have a copy, make it executable:

```
chmod +x /bin/apt-cyg
```

Optionally place apt-cyg in a bin/ folder on your `$PATH`.

Then use apt-cyg, for example:

```
apt-cyg install nano
```

## Contributing

This project has been re-published on <a href='https://gitlab.com/scrptd/apt-cyg/'>GitLab</a> to make contributing easier as it was removed from Google Code. Feel free to fork and modify this script.

## Changelog

### v0.60

* Added chmod +x to post install script

### v0.59

* Added xz archives support

### v0.58

* Added multiarch support

### v0.57

* Tagged Google Code SVN repo status
